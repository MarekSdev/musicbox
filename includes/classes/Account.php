<?php 
	class Account {

		private $con;
		private $errorArray;

		public function __construct($con) {
			$this->con = $con;
			$this->errorArray = array();
		}

		public function login($username, $password) {
			$password = md5($password);

			$query = mysqli_query($this->con, "SELECT * FROM users WHERE username='$username' AND password='$password'");

			if (mysqli_num_rows($query) == 1) {
				# code...
				return true;
			} else {
				array_push($this->errorArray, Constants::$loginFailed);
				return false;
			}
		}

		public function register($username, $firstName, $lastName, $email, $emailConfirmation, $password, $passwordConfirmation) {
			$this->validateUserName($username);
			$this->validateFirstName($firstName);
			$this->validateLastName($lastName);
			$this->validateEmails($email, $emailConfirmation);
			$this->validatePasswords($password, $passwordConfirmation);

			if (empty($this->errorArray) == true) {
				# check if no errors
				// insert into db
				return $this->insertUserDetails($username, $firstName, $lastName, $email, $password);
			} else {
				return false;
			}
		}

		public function getError($error) {
			if (!in_array($error, $this->errorArray)) {
				# check if any error exist in error array
				$error = "";
			}
			return "<span class='errorMessage'>$error</span>";
		}

		private function insertUserDetails($username, $firstName, $lastName, $email, $password) {
			$encryptedPassword = md5($password);
			$profilePic = "assets/images/profile-pics/default.png";
			$date = date("Y-m-d");

			$result = mysqli_query($this->con, "INSERT INTO users VALUES ('', '$username', '$firstName', '$lastName', '$email', '$encryptedPassword', '$date', '$profilePic')");
			return $result;
		}

		private function validateUserName($username) {
			if (strlen($username) > 25 || strlen($username) < 5) {
				# validate username length
				array_push($this->errorArray, Constants::$usernameCharacters);
				return;
			}
			//TODO: check if username exists
			$checkUsernameQuery = mysqli_query($this->con, "SELECT username FROM users WHERE username='$username'");
			if (mysqli_num_rows($checkUsernameQuery) != 0) {
				# code...
				array_push($this->errorArray, Constants::$usernameTaken);
				return;
			}
		}

		private function validateFirstName($firstName) {
			if (strlen($firstName) > 25 || strlen($firstName) < 2) {
				# validate first name length
				array_push($this->errorArray, Constants::$firstNameCharacters);
				return;
			}	
		}

		private function validateLastName($lastName) {
			if (strlen($lastName) > 25 || strlen($lastName) < 2) {
				# validate last name length
				array_push($this->errorArray, Constants::$lastNameCharacters);
				return;
			}
		}

		private function validateEmails($email, $emailConfirmation) {
			if ($email != $emailConfirmation) {
				# check if emails match
				array_push($this->errorArray, Constants::$emailsDoNotMatch);
				return;
			}

			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				# check if email is in correct format
				array_push($this->errorArray, Constants::$emailInvalid);
				return;
			}

			//check that email hasn't allready been used
			$checkEmailQuery = mysqli_query($this->con, "SELECT email FROM users WHERE email='$email'");
			if (mysqli_num_rows($checkEmailQuery) != 0) {
				# code...
				array_push($this->errorArray, Constants::$emailTaken);
				return;
			}

			
		}

		private function validatePasswords($password, $passwordConfirmation) {
			if ($password != $passwordConfirmation) {
				# check if passwords match
				array_push($this->errorArray, Constants::$passwordsDoNotMatch);
				return;
			}
			if (preg_match('/[^A-Za-z0-9]/', $password)) {
				array_push($this->errorArray, Constants::$passwordNotAlphanumeric);
				return;
			}
			if (strlen($password) > 30 || strlen($password) < 5) {
				# validate username length
				array_push($this->errorArray, Constants::$passwordCharacters);
				return;
			}
		}
	}

?>