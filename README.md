# Personal musicbox

SPA (Simple Page Application) that mimic's Spotify.

### TODO

```
- fix shuffle bug
- set song progress default value to 0.00 (css)
- add new welcome page (currently = browse)
- add table schemas and manual phpmyadmin instructions
- add php script to insert dummy data into tables
```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What things you need to run the application on your local machine.

```
XAMPP (Linux, Win, OSX) - https://www.apachefriends.org/index.html
PHP 5.3 or higher (to use MySQLi functions to access MySQL database servers)
```

### Set-up

A step by step guide that tell you how to get a development env running

Clone the repo to your machine 

```
MacOS : git clone <link> /Applications/XAMPP/xamppfiles/htdocs
TODO: Add repo link and check Linux and Win xamp htdocs dir location
```

Run XAMPP and start all servers

```
MacOS : sudo /Applications/XAMPP/xamppfiles/xampp start
TODO: add linux / win command
```

Create MySQL database

```
TODO: find easiest way to create phpmyadmin database (script?)
```


Create tables and insert dummy data (to manually insert tables via phpmyadmin view MySQL Tables section)

```
TODO: create sql or php file to insert tables & dummy data
```

Run application

```
MacOS: open -a safari http://localhost/MusicBox/register.php
TODO: add win/linux version
```


You should now create an account to login in and explore the features of MusicBox.

Stop XAMPP servers

```
MacOS: sudo /Applications/XAMPP/xamppfiles/xampp stop
TODO: add win/linux version
```


## MySQL Tables

users

```
CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(30) NOT NULL,
  `signUpDate` datetime NOT NULL,
  `profilePic` varchar(500) NOT NULL
);
```

|id|username|firstName|lastName|email|password|signUpDate|profilePic|
|---|---|---|---|---|---|---|---|
|int (A_I) primary|varchar(25)|varchar(50)|varchar(50)|varchar(200)|varchar(30)|datetime|varchar(500)|

albums

```
CREATE TABLE IF NOT EXISTS `albums` (
`id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `artist` int(11) NOT NULL,
  `genre` int(11) NOT NULL,
  `artworkPath` varchar(500) NOT NULL
);
```

|id|title|artist|genre|artworkPath|
|---|---|---|---|---|
|int (A_I) primary|varchar(250)|int|int|varchar(200)|

artists

```
CREATE TABLE IF NOT EXISTS `artists` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
);
```

|id|name|
|---|---|
|int (A_I) primary|varchar(50)|

genres

```
CREATE TABLE IF NOT EXISTS `genres` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
);
```

|id|name|
|---|---|
|int (A_I) primary|varchar(50)|

playlists

```
CREATE TABLE IF NOT EXISTS `playlists` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `owner` varchar(50) NOT NULL,
  `dateCreated` datetime NOT NULL
);
```

|id|name|owner|dateCreated|
|---|---|---|---|
|int (A_I) primary|varchar(50)|varchar(50)|datetime|

playlistSongs

```
CREATE TABLE IF NOT EXISTS `playlistSongs` (
`id` int(11) NOT NULL,
  `songId` int(11) NOT NULL,
  `playlistId` int(11) NOT NULL,
  `playlistOrder` int(11) NOT NULL
);
```

|id|songId|playlistId|playlistOrder|
|---|---|---|---|
|int (A_I) primary|int|int|int|

songs

```
CREATE TABLE IF NOT EXISTS `Songs` (
`id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `artist` int(11) NOT NULL,
  `album` int(11) NOT NULL,
  `genre` int(11) NOT NULL,
  `duration` varchar(8) NOT NULL,
  `path` varchar(500) NOT NULL,
  `albumOrder` int(11) NOT NULL,
  `plays` int(11) NOT NULL
);
```

|id|title|artist|album|genre|duration|path|albumOrder|plays|
|---|---|---|---|---|---|---|---|---|
|int (A_I) primary|varchar(250)|int|int|int|varchar(8)|varchar(500)|int|int|

## Deployment

Coming soon.. maybe


## Built With

* HTML5 
* CSS
* PHP
* AJAX
* jQuery
* MySQL 


## Author

* **MarekS**  

## License

This project is licensed under the MIT License.
## Acknowledgments

* todo


