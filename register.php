<?php
	include("includes/config.php");
	include("includes/classes/Account.php");
	include("includes/classes/Constants.php");

	$account = new Account($con);

	include("includes/handlers/register-handler.php");
	include("includes/handlers/login-handler.php");

	function getInputValue($name) {
		if(isset($_POST[$name])) {
			echo $_POST[$name];
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to Slotify</title>
	<link rel="stylesheet" type="text/css" href="assets/css/register.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="assets/js/register.js"></script>
</head>
<body>
	<?php 
	if (isset($_POST['registerButton'])) {
		echo '<script>
			$(document).ready(function() {
				$("#loginForm").hide();
				$("#registerForm").show();
			});
			</script>';
	} else {
		echo '<script>
			$(document).ready(function() {
				$("#loginForm").show();
				$("#registerForm").hide();
			});
			</script>';
	}
	?>
	

	<div id="background">
		<div id="loginContainer">
			<div id="inputContainer">
				<form id="loginForm" action="register.php" method="POST">
					<h2>Login to your account</h2>
					<p>	
						<?php echo $account->getError(Constants::$loginFailed); ?>
						<label for="loginUsername">Username</label>
						<input type="text" id="loginUsername" name="loginUsername" placeholder="e.g bartSimpson" value="<?php getInputValue('loginUsername'); ?>"required>
					</p>
					<p>
						<label for="loginPassword">Password</label>
						<input type="password" id="loginPassword" name="loginPassword" placeholder="password" required>
					</p>
					<button type="submit" name="loginButton">LOG IN</button>

					<div class="hasAccountText">
						<span id="hideLogin">Don't have an account yet? Signup here.</span>
					</div>
				</form>

				<form id="registerForm" action="register.php" method="POST">
					<h2>Create your free account</h2>
					<p>
						<?php echo $account->getError(Constants::$usernameCharacters); ?>
						<?php echo $account->getError(Constants::$usernameTaken); ?>
						<label for="username">Username</label>
						<input type="text" id="username" name="username" placeholder="e.g bartSimpson" value="<?php getInputValue('username'); ?>" required>
					</p>
					<p>
						<?php echo $account->getError(Constants::$firstNameCharacters); ?>
						<label for="firstName">First Name</label>
						<input type="text" id="firstName" name="firstName" placeholder="e.g Bart" value="<?php getInputValue('firstName'); ?>" required>
					</p>
					<p>
						<?php echo $account->getError(Constants::$lastNameCharacters); ?>
						<label for="lastName">Last Name</label>
						<input type="text" id="lastName" name="lastName" placeholder="e.g Simpson" value="<?php getInputValue('lastName'); ?>"  required>
					</p>
					<p>
						<?php echo $account->getError(Constants::$emailInvalid); ?>
						<?php echo $account->getError(Constants::$emailTaken); ?>
						<?php echo $account->getError(Constants::$emailsDoNotMatch); ?>
						<label for="email">E-mail</label>
						<input type="email" id="email" name="email" placeholder="e.g bart@gmail.com" value="<?php getInputValue('email'); ?>" required>
					</p>
					<p>
						<label for="emailConfirmation">Confirm E-mail</label>
						<input type="email" id="emailConfirmation" name="emailConfirmation" placeholder="e.g bart@gmail.com" required>
					</p>
					<p>
						<?php echo $account->getError(Constants::$passwordsDoNotMatch); ?>
						<?php echo $account->getError(Constants::$passwordNotAlphaNumeric); ?>
						<?php echo $account->getError(Constants::$passwordCharacters); ?>
						<label for="password">Password</label>
						<input type="password" id="password" name="password" placeholder="your password" required>
					</p>
					<p>
						<label for="passwordConfirmation">Confirm Password</label>
						<input type="password" id="passwordConfirmation" name="passwordConfirmation" placeholder="confirm your password" required>
					</p>
					<button type="submit" name="registerButton">SIGN UP</button>
					<div class="hasAccountText">
						<span id="hideRegister">Already have an account? Log in here.</span>
					</div>
				</form>

			</div>
			
			<div id="loginText">
				<h1>Musicbox</h1>
				<h2>For personal use</h2>
				<ul>
					<li>Search music via song/artist/album</li>
					<li>Create your own playlists</li>
					<li>Enjoy!</li>
				</ul>
			</div>


		</div>
	</div>
	
</body>
</html>